function getFieldIndexes() {

}

function getRandom(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getRandomFromArray( arr ) {
	const random = getRandom( arr.length );
	return {
		'element': arr[ random ],
		'index'  : arr.indexOf( arr[ random ] )
	};
}

function getRandomElementFromArray( arr ) {
	return arr[ getRandom( arr.length ) ];
}

function getRandomIndexFromArray( arr ) {
	return arr.indexOf( arr[ getRandom( arr.length ) ] );
}

function getRow( item ) {
	return item.getAttribute( 'row' );
}

function getColumn( item ) {
	return item.getAttribute( 'column' );
}

function getHorozontalSpace( column, maxColumns ) {
	return ( maxColumns - column ) + 1;
}

function isHorizontalSpace( column, maxColumns, word ) {

	return getHorozontalSpace( column, maxColumns ) >= word.length ? true : false;

}

function getVerticalSpace( row, maxRows ) {
	return ( maxRows - row ) + 1;
}

function isVerticalSpace( row, maxRows, word ) {

	return getVerticalSpace( row, maxRows ) >= word.length ? true : false;

}

function addWord( fields, fieldIndex, word, data ) {

	const element = fields[ fieldIndex ];
console.log(element);
	if ( isHorizontalSpace( getColumn( element ), data.columns, word ) ) {

		let
		wordLength  = word.length,
		currentElement = element;

		for ( let i = 0; i < wordLength; i++ ) {
			currentElement.childNodes[0].value = word[ i ];
			currentElement = currentElement.nextElementSibling;
		}

		data.fieldIndexes.splice( fieldIndex, 1 );

	} else if ( isVerticalSpace( getRow( element ), data.rows, word ) ) {

		let
		wordLength = word.length,
		column     = element.getAttribute( 'column' ),
		row        = element.getAttribute( 'row' );

		for ( let i = 0; i < wordLength; i++ ) {
			element.parentNode.querySelector( `[column="${column}"][row="${row++}"]` ).childNodes[0].value = word[ i ];
		}

		data.fieldIndexes.splice( fieldIndex, 1 );

	} else {
		console.log('no space!');
	}

}

export default function( data ) {

	const
	fields      = Array.from( document.querySelectorAll( '.fields__item' ) ),
	words       = data.words,
	wordsLength = words.length;

	let	fieldIndex;

	for ( let i = 0; i < wordsLength; i++ ) {

		fieldIndex = getRandom( data.fieldIndexes.length );
		addWord( fields, fieldIndex, words[ i ], data );

	}

}
